package model

import "time"

type MyTodo struct {
	ID     string
	Text   string
	Done   MyBoolean
	DoneAt time.Time
	User   *User
}
