package graph

import "gitlab.com/mvalley/gqlgen-todo/graph/model"

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

type Resolver struct {
	todos []*model.MyTodo
}
