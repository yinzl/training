# gqlgen

## 自我介绍
1. 姓名
2. 负责的项目
3. 展示产品（产品关键技术）

**这堂课我们讲如何用 go 语言实现 graphql 后端 --> gqlgen**

## 复习 GraphQL

**你们之前上过 Kcat 的 GraphQL 课程，哪位同学告诉下我什么是 GraphQL？（点三个人回答）**

1. RESTful 是我们设计 API 的一种规范，规范把数据当作资源，你可以通过 GET 获取资源，通过 POST 创建资源
2. GraphQL 和 RESTful 一样也是一种规范，类似 RESTful 你可以通过 Query 获取数据，Mutation 操作数据。
3. **优点**GraphQL 相对 RESTful 的优势：
   1. 前端可以只取自己想要的数据，而不是像 REST 那样一股脑全返回。
   2. 是**强类型**的，就是说前端必须传递事先定好类型的参数，不然编译期就报错了。这带来的好处就是**你的 API 就是你的接口文档，不需要去维护接口文档**。
4. **缺点**
   1. 复杂度相对高些，而且资料文档比较少
   2. 如果真的做到按需，一个 field 一个 resolver，那性能会很差。所以我们现在实现时后端是没有实现按需取的。

## gqlgen 基础

**回到我们本堂课要讲的 gqlgen，gqlgen 是什么呢？**
**gqlgen 是一个帮助快速构建 GraphQL 服务的 Go 语言库**  
**通过这个库我们可以不去管怎么实现 GraphQL 服务，只需定义好 schema 通过库生成代码，然后实现业务逻辑就可以了**
**那现在我们就来学习如何使用这个库**
**首先我们通过一个示例项目来快速学习下基础用法，后面我们再来学习下如何在实际项目中使用**

**讲解 `init` 项目**
 1. 打开官方文档 https://gqlgen.com/
 2. 敲命令拉下项目
     ```bash
     mkdir todo
     go mod init gitlab.com/mvalley/todo
     go run github.com/99designs/gqlgen init -> *这个命令可以初始化一个 gqlgen 项目*
     ```
   
 3. 这个项目是做什么的 
     备忘录，功能
    
 4. 看 schema 解释 schema 内容
     **`!` 是什么意思？**
     **`[!]!` 是什么意思？**
     **input 是什么意思?**
     **哪些接口放 `Query` 里，哪些接口放 `Mutation` 里面？**
   
 5. 看 model
     **`json:"text"` 是什么？以及有啥用？**
    这里的 model 是 gqlgen 根据我们定义的 schema 生成的模型 -> 修改 schema 验证一下
    当然我们可以不用他生成的模型，我们可以绑定自己的模型 -> 新建文件夹演示绑定模型，绑定的效果通过说明生成的 model 中没有绑定的模型，然后顺势跳到 `接口` 接口中的模型变成我们自己定义的模型
     *这里顺便把 go generate 加上*
     *模型生成在 `model` 里，那接口在哪呢？*
 
 6. 看 schema.resolvers 
    **这就是我们实现业务逻辑的地方**
    **实现接口，演示按需取**
     
 7. resolver
      **将实现的接口链接到框架**
      **一般用来放一些类似数据库连接、Redis 连接等资源** 
    
 8. 看 gqlgen.yml
     配置文件各个字段解释
     以及各个字段修改一下，看看效果
     **演示模型绑定**
     演示 `go generate`
 
 9.  generated
     过一下

 10. server 
     这里就是我们程序的主入口
     *打开 playground 演示*
     实现一下服务，然后再演示

 11. 总结
   - *下面我们总结下，一个 gqlgen 项目一定有一个 schema，这个 schema 定义我们的接口以及模型，然后通过 schema 就可以生成我们的项目，我们只需要去实现接口业务逻辑即可。这就是 `gqlgen` 的强大之处，我们只需关注业务而不需要关注底层代码。另外我们可以通过配置文件来控制生成代码的路径以及模型绑定等*
 
 11. 回顾
     **每个文件问下是做什么的**

## gqlgen 实操

**接下来我们来学习我们实际工作中如何使用他，我们来写个加强版备忘录项目**

1. 拉下加强版备忘录 `init` 
2. 看 README，讲解功能
3. 两两分组讨论接口设计以及写出 Schema
4. 随机抽 3 组左右讲一下

**操作**
- 写 schema
  - 自定义标量
  - 参数 `input`，`input` 只能作为方法的参数模型
- 写配置文件
  - 模型绑定
  - **问配置字段作用**
- 完成每个接口并演示
  - 使用 Playground debug
  - **带着写两个接口，然后让他们自己实现剩余接口，最后再全部实现**
  - 点评几个

**大家说用什么数据结构比较好**

TodosMap map[string]*models.Todo

# Twirp

**这堂课我们来学习另外一个我们工作中要用到的框架，Twirp**

## Twirp 介绍

Twirp 是什么？
- Twirp 是一个简单的基于 protobuf 的 RPC 框架

**那有的同学要问 RPC 又是什么了，有同学能解释下 RPC 是什么吗？**

RPC 是什么？
- RPC 是远程过程调用，也就是让客户端像调用本地方法一样去调用远程服务端的方法。

**Twirp 就是一个简单的基于 protobuf 的 RPC 框架。protobuf 是类似 JSON 一样的一个数据格式，我们之后会详细介绍的，暂时不管。也可类比我们刚才学的 gqlgen，gqlgen 是一个帮助快速构建 GraphQL 服务的 Go 语言库，那 Twirp 就是一个帮助快速构建 RPC 服务的 Go 语言库**

**讲解 `init` 项目**
- 拉下事先准备好的 `init` 项目（简单版本 `备忘录`）
- 跑 Test 看效果
- `.proto` 文件
  - 定义接口以及模型
- 两个自动生成文件
- 业务实现代码
- 服务入口以及测试代码

**Protobuf**
是什么？
**是类似 JSON、XML 这样的数据格式。但是它的体积比 JSON、XML 小很多，同时性能是JSON、XML 的几十倍。怎么做到的呢？是编码方式做了很大的优化，它一个小于 128 的数只需要 1 个字节就可以保存，而 JSON 需要 4 个字节。**

- 基础知识
  - Defining A Message Type
  - Using Other Message Types
  - Importing Definitions
  - Defining Services
  <!-- - 删除了 required 和 optional，为什么？
    - 保持其向前向后兼容性 -->
- 与 JSON 比较
  - 为什么性能比 JSON 好

## Twirp 实操

**接下来我们来学习我们实际工作中如何使用他，我们和学习 gqlgen 一样来写个加强版备忘录**

1. 拉下加强版备忘录 `init` 
2. 看 README，讲解功能
3. 两两分组讨论写出 `Proto`
4. 随机抽 3 组左右讲一下

**操作**
- 写 proto
  - 注意使用 wrapper 类型来表示 optional
- 生成代码
- 完成每个接口并演示
  - **带着写一个接口，然后让他们自己实现剩余接口**
  - 点评几个

# API Gateway

**在微服务的理念中，如果前端想要对后端进行请求，需要为每个微服务单独开发一套客户端。这不仅在开发时耗费时间，而且对这些客户端的管理也极其繁琐。因此，我们可以提出一个 API Gateway 的概念**

// TODO: 再仔细阅读
1. 看介绍
**你们打开这个文档先自行阅读下**
在微软的这篇文章中有详细介绍(https://docs.microsoft.com/en-us/azure/architecture/patterns/gateway-routing)
**有同学能给我总结下讲了啥吗？**

2. 看案例
**你们再打开这个实际案例自行阅读下**
https://outcrawl.com/go-graphql-gateway-microservices

3. **讲解 API Gateway 思想**
  
4. 提问
**这种模式有什么缺点？**

1. 两两分组讨论将各自的 Todo Pro 项目修改为 API Gateway 模式
**随机抽 3 组左右讲一下**

5. 共用模型
- **提问 transcode 这个步骤有没更好的解决方案？**
- **需要用到我们 gqlgen 的模型绑定**
- **演示绑定**
- 演示共用模型改造
 - 基本类型不匹配问题
 - proto3 中，参数无 required 和 optional 的概念
   - 自定义标量
   - 为什么 proto3 去掉 required 和 optional 的概念？


### 登陆自己的 Github 
schema and proto

### 添加环境变量

### 安装 gqlgen
go get -u github.com/99designs/gqlgen

### 安装 protoc
```bash
sudo apt-get update
sudo apt install protobuf-compiler
```

### 安装 protoc-gen-go 和 protoc-gen-twirp 插件
```bash
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u github.com/twitchtv/twirp/protoc-gen-twirp
```
确定 PATH 环境变量中包含了 $GOPATH/bin
```
export PATH=$PATH:$GOPATH/bin
```

### Protobuf
https://developers.google.cn/protocol-buffers/docs/proto3

### API Gateway
https://docs.microsoft.com/en-us/azure/architecture/patterns/gateway-routing

https://outcrawl.com/go-graphql-gateway-microservices



## 附录
```
scalar Time 

type Query {
    todos(
        userID: String!
        state: State!
    ): [Todo!]
}

type Mutation {
    createTodo(
        userID: String!
        text: String!
        planTime: Time
    ): Todo!

    updateTodo(
        input: TodoInput!
    ): Todo!

    deleteTodo(
        userID: String!
        id: String!
    ): Boolean!

    setTodoDone(
        userID: String!
        id: String!
    ): Boolean!
}

input TodoInput {
    userID: String!
    id: ID!
    text: String
    planTime: Time
    done: Boolean
    doneAt: Time
}

type Todo {
    userID: String!
    id: ID!
    text: String!
    planTime: Time
    done: Boolean!
    doneAt: Time
}

enum State {
    ALL
    DONE
    TODO
}

type User {
    id: String!
    name: String!
}
```


```
syntax = "proto3";

package io.mvalley.todo_pro_twirp;

import "google/protobuf/timestamp.proto";
import "google/protobuf/wrappers.proto";
option go_package = "todo_pro_twirp";

service TodoProTwirp {
    rpc GetTodos(GetTodosRequest) returns (TodosResponse);
    rpc CreateTodo(CreateTodoRequest) returns (Todo);
    rpc UpdateTodo(UpdateTodoRequest) returns (Todo);
    rpc DeleteTodo(DeleteTodoRequest) returns (google.protobuf.BoolValue);
    rpc SetTodoDone(SetTodoDoneRequest) returns (google.protobuf.BoolValue);
}

message GetTodosRequest {
    string user_id = 1;
    State state = 2;
}

message TodosResponse {
    repeated Todo todos = 1;
}

message Todo {
    string user_id = 1;
    string id = 2;
    string text = 3;
    google.protobuf.Timestamp plan_time = 4;
    bool done = 5;
    google.protobuf.Timestamp done_at = 6;
}

enum State {
    ALL = 0;
    DONE = 1;
    TODO = 2;
} 

message CreateTodoRequest {
    string user_id = 1;
    string text = 2;
    google.protobuf.Timestamp plan_time = 4;
}

message UpdateTodoRequest {
    string user_id = 1;
    string id = 2; 
    google.protobuf.StringValue text = 3;
    google.protobuf.Timestamp plan_time = 4;
    google.protobuf.BoolValue done = 5;
    google.protobuf.Timestamp done_at = 6;
}

message DeleteTodoRequest {
    string user_id = 1;
    string id = 2; 
}

message SetTodoDoneRequest {
    string user_id = 1;
    string id = 2; 
}
```
