
> 1. script, 不是大纲，必须按顺序来；要做到，看到关键词就能反应过来该怎么具体表达。
> 2. 代码的部分，再具体操练，所有的知识点，必须记录下来
> 3. 设计好你的作业们，包括课间作业和大作业，Specification 必须非常具体。


# Script

- 自我介绍 + 引子
  - 姓名，Codename [写黑板]
  - **猜猜是为什么叫这个名字？** 
    - 提示：是两个英文单词融合在一起了。incremental, increase -> inceasetal
    - 提醒大家千万别写错
  - 负责的项目
    - profile
      - 展示一下产品
    - 用到了关键技术，就是 graphql 和 Twirp，以及两者如何协作
  - *这堂课我们讲 go 语言如何实现 graphql 后端 --> gqlgen*

- gqlgen
  - 先复习一下 GraphQL **你们昨天上过 Kcat 的 GraphQL 课程，哪位同学告诉下我什么是 GraphQL？**
      - 点 3 个同学左右
    - 总结什么是 GraphQL 以及其与 Rest 区别
  - gqlgen 是什么？
    - 是一个帮助快速构建 GraphQL 服务的 Go 语言库。

  ↓   *市面上其实有很多类似的库，比如 `gophers`、`graphql-go` 以及 `thunder` 等等，那我们为啥偏偏选择了 gqlgen 呢？总结来说的话，就是他有如下几个优点*
  - 为什么选择 gqlgen
    - schema first 以及解释
    - [type safety](http://www.imooc.com/wenda/detail/593852) 以及解释
    - 代码生成更方便，可以直接生成枚举和 input

  ↓   *那现在我们来看为什么用 gqlgen 写就是这么爽，首先我们通过一个示例项目来快速学习下基础用法，后面我们再来学习下如何在实际项目中使用*
  - 讲解 `init` 项目
    - 敲命令拉下项目
      - 命令
    - 这个项目是做什么的 
      - 备忘录，功能
    - 看 schema 
      - 解释 schema 内容
      - 演示修改 schema 后重新生成代码
        - 演示 `go generate`
      - *一般我们要去看一个项目，我们首先是看他的文档，也就是 README，这个示例项目很简单，我们就直接看到他的接口定义就行，作为 GraphQL 项目，接口定义就在他的 schema 里面。大家打开 schema 文件，就是那个后缀为 `.graphqls` 的文件*
      - **`!` 是什么意思？**
      - **`[!]!` 是什么意思？**
      - **哪些接口放 `Query` 里，哪些接口放 `Mutation` 里面？**
    - model
      - **`json:"text"` 是什么？以及有啥用？**
      - *模型生成在 `model` 里，那接口在哪呢？*
    - schema.resolvers 
      - resolver 的结构
    - gqlgen.yml
      - 配置文件各个字段解释
    - server 以及 generated
      - 过一下
    - 总结
      - *下面我们总结下，一个 GraphQL 项目一定有一个 schema，这个 schema 定义我们的接口以及模型，然后通过 schema 就可以生成我们的项目，我们只需要去实现接口业务逻辑即可。这就是 `gqlgen` 的强大之处，我们只需关注业务而不需要关注底层代码。另外我们可以通过配置文件来控制生成代码的路径以及模型绑定等*
      - 回顾
        - **每个文件问下是做什么的**

  ↓   *接下来我们来学习我们实际工作中如何使用他，我们来写个加强版备忘录项目*
  - 完成加强版 `备忘录` 项目
    - 产品设计（**交互式，问：如果要实现一个备忘录需要实现哪些功能？**）
      - 实现如下功能
        - 创建备忘录
          - 做什么？
          - 什么时候做？
          - 是否提醒？
        - 备忘录列表
          - 未完成列表（按时间顺序由近到远排序）
            - 编辑
            - 删除
              - 单个
              - 全部
            - 设置已完成
              - 单个
              - 全部
          - 已完成列表（按时间顺序由近到远排序）
            - 删除
              - 单个
              - 全部
          - 分页功能
        - 提醒功能
    - 接口设计（**先让他们 2人 分组讨论设计接口以及数据模型，随机 3 组左右讲一下，直接写出 schema，而后交互式把接口以及数据模型写出来**）
      - createTodo
      - todos                
      - updateTodo
      - deleteTodos
      - setTodoDone
      - alert
    - go mod 初始化项目
      - 目录结构
    - 写 schema
      - 自定义标量
      - 参数可以用 `input`，`input` 只能作为方法的参数模型
      - **复习 GraphQL**
    - 写配置文件
      - 模型绑定
      - **问配置字段作用**
    - 完成每个接口并演示
      - 使用 Playground debug
      - **带着写两个接口，然后让他们自己实现剩余接口，最后再全部实现**
      - 点评几个


↓   *这堂课我们来学习另外一个我们工作中要用到的框架，Twirp*
- Twirp
  - Twirp 是什么？
    - Twirp 是一个简单的基于 protobuf 的 RPC 框架
  
  ↓   *那有的同学要问 RPC 又是什么了*
  - RPC 是什么？
    - **有同学能解释下 RPC 是什么吗？**
    - protobuf 是什么
    - RPC 是远程过程调用，也就是让客户端像调用本地方法一样去调用远程服务端的方法。
    - *Twirp 就是一个简单的基于 protobuf 的 RPC 框架。protobuf 是类似 JSON 一样的一个数据格式，我们之后会详细介绍的，暂时不管。也可类比我们刚才学的 gqlgen，gqlgen 是一个帮助快速构建 GraphQL 服务的 Go 语言库，那 Twirp 就是一个帮助快速构建 RPC 服务的 Go 语言库*

  - 为什么选择 Twirp
    - Twirp 可以说是继承了 gRPC 优点，规避了 gRPC 的不足

  - 讲解 `init` 项目
    - 拉下事先准备好的 `init` 项目（简单版本 `备忘录`）
    - 跑通看效果，然后讲解
    - `.proto` 文件
      - 定义接口以及模型
    - 两个自动生成文件
    - 业务实现代码
    - 服务入口以及测试代码

  - Protobuf
    - 基础知识
      - Defining A Message Type
      - Using Other Message Types
      - Importing Definitions
      - Defining Services
    - 与 JSON 比较
      - 为什么性能比 JSON 好

  - 使用 Twirp 完成加强版 `备忘录` 项目
    - 接口定义
      - **定义两个，其余自己定义**
    - 接口实现
      - **实现两个，其余自己实现**
    - 接口测试
      
- API Gateway
  - 带着看案例
  - **这种模式有什么优点？**
  - **这种模式有什么缺点？**
  
- 课间作业
  - **参考 API Gateway 章节的案例，将我们之前示例的 `备忘录` 改成 API Gateway 模式**

- 共用模型
  - **提问 transcode 这个步骤有没更好的解决方案？**
  - 演示共用模型改造
    - 基本类型不匹配问题
    - proto3 中，参数无 required 和 optional 的概念
      - 自定义标量
      - 为什么 proto3 去掉 required 和 optional 的概念？
    
- config

- 讲解我们实际新增一个微服务的流程
  - `Rome` 名字的由来
  - 让他们动手把 `加强版备忘录` 挂载到 `Rome`

# 大作业
  - 个人博客后台