# gqlgen & Twirp

<!-- TOC -->

- [gqlgen & Twirp](#gqlgen--twirp)
	- [1. gqlgen](#1-gqlgen)
		- [1.1. gqlgen 是什么？](#11-gqlgen-是什么)
		- [1.2. 为什么选择 gqlgen?](#12-为什么选择-gqlgen)
		- [1.3. 快速开始](#13-快速开始)
		- [1.4. 配置文件](#14-配置文件)
		- [1.5. go generate](#15-go-generate)
		- [1.6. 模型绑定](#16-模型绑定)
		- [1.7. 自定义标量](#17-自定义标量)
		- [1.8. 官方文档 & Github](#18-官方文档--github)
		- [1.9. gqlgen 使用常见的“坑”](#19-gqlgen-使用常见的坑)
	- [2. Twirp](#2-twirp)
		- [2.1. RPC 是什么？](#21-rpc-是什么)
		- [2.2. Twirp 是什么？](#22-twirp-是什么)
		- [2.3. 为什么选择 Twirp](#23-为什么选择-twirp)
		- [2.4. 环境配置](#24-环境配置)
		- [2.5. 快速开始](#25-快速开始)
		- [2.6. 官方文档 & Github](#26-官方文档--github)
		- [2.7. Twirp 使用常见的“坑”](#27-twirp-使用常见的坑)
	- [3. Protobuf](#3-protobuf)
		- [3.1. 是什么？](#31-是什么)
		- [3.2. Defining A Message Type](#32-defining-a-message-type)
		- [3.3. Using Other Message Types](#33-using-other-message-types)
		- [3.4. Importing Definitions](#34-importing-definitions)
		- [3.5. Defining Services](#35-defining-services)
		- [3.6. 官方文档 & Github](#36-官方文档--github)
		- [3.7. Protobuf 使用常见的“坑”](#37-protobuf-使用常见的坑)
	- [4. gqlgen & Twirp](#4-gqlgen--twirp)
		- [4.1. RPC 和微服务](#41-rpc-和微服务)
		- [4.2. API Gateway](#42-api-gateway)
			- [4.2.1. 参考文档](#421-参考文档)
			- [4.2.2. 一个具体案例](#422-一个具体案例)
		- [4.3. 课间练习](#43-课间练习)
		- [4.4. 共用模型](#44-共用模型)
		- [4.5. proto Wrapper Values](#45-proto-wrapper-values)

<!-- /TOC -->

## 1. gqlgen

### 1.1. gqlgen 是什么？

是一个帮助快速构建 GraphQL 服务的 Go 语言库。

### 1.2. 为什么选择 gqlgen?

![Comparison](./gqlgen-feature-comparison.png)

可以看到 gqlgen 比其他几个库牛逼很多，特别是支持 `类型绑定`、`枚举生成` 以及 `Input生成` 等特点是非常的方便的。

### 1.3. 快速开始

1. 安装库：

```bash
$ go get github.com/99designs/gqlgen
```

2. 初始化一个示例项目：

```bash
$ mkdir gqlgen-todos
$ cd gqlgen-todos
$ go mod init gitlab.com/mvalley/gqlgen-todos
$ go run github.com/99designs/gqlgen init
```

- 文件说明：

  - `gqlgen.yml` - 这个文件是 gqlgen 的配置文件，配置文件内容下节会细讲
  - `models_gen.go` - 生成的模型
  - `schema.graphqls` - graphQL schema
  - `schema.resolvers.go` - 在这个文件中实现业务逻辑

3. 实现业务逻辑：

- 在 `resolver.go` 里添加 `todos`，如下：
```go
type Resolver struct {
	todos []*model.Todo
}
```

- 在 `schema.resolvers.go` 里添加业务实现代码，如下：
```go
func (r *mutationResolver) CreateTodo(ctx context.Context, input model.NewTodo) (*model.Todo, error) {
	todo := &model.Todo{
		Text:   input.Text,
		ID:     fmt.Sprintf("T%d", rand.Int()),
		User: &model.User{ID: input.UserID, Name: "user " + input.UserID},
	}
	r.todos = append(r.todos, todo)
	return todo, nil
}

func (r *queryResolver) Todos(ctx context.Context) ([]*model.Todo, error) {
	return r.todos, nil
}
```

4. 运行看效果：

- 执行命令 `go run server.go`，并打开 [Playground](http://localhost:8080)

- 在 Playground 里面输入如下查询：

```graphql
mutation createTodo {
    createTodo(input:{text:"todo", userId:"1"}) {
        user {
            id
        }
        text
        done
    }
}

query findTodos {
  	todos {
        text
        done
        user {
            name
        }
    }
}
```

### 1.4. 配置文件

在上面快速开始中，我们的 schema 文件是个 `.graphqls` 后缀的文件，一般我们写的 schema 是以 `.graphql` 为后缀的文件。我们修改文件后缀为 `.graphql` 并重新生成代码。

gqlgen 生成代码命令：
```bash
go run github.com/99designs/gqlgen generate
```

重新生成代码失败，为什么呢？

那是因为配置文件 `gqlgen.yml` 中配置只扫描 `graph` 包中所有后缀为 `.graphqls` 的 schema，我们只需要在配置文件中添加同时扫描后缀为 `.graphql` 的 schema 文件即可。修改如下：

```yml
schema:
  - graph/*.graphqls
  - graph/*.graphql
```

再次执行生成代码命令，重新生成代码成功。

这就是配置文件的作用，可以让你配置从哪里读取 schema 文件，生成的 server 或者 model 代码的文件路径和文件名等。可以仔细看看示例中配置文件 `gqlgen.yml` 中各种配置的说明。

**如果生成代码出问题，首先检查配置文件**

### 1.5. go generate

每次执行重新生成代码命令很烦，所以我们可以使用一个快捷方式 `go generate`。

我们只需在项目根目录添加一个 `gen.go` 文件，并在文件中添加如下代码：

```go
package main

// 重点是下面这行注释
//go:generate go run github.com/99designs/gqlgen generate
```

之后每次重新生成代码只需执行 `go generate` 即可。

`go generate` 可以执行任何你想执行的命令

### 1.6. 模型绑定

当我们不希望用 gqlgen 自动生成的模型，而是用我们自己定义的模型该怎么办呢？

在 model 文件夹中添加一个 `my_model.go` 文件，在文件中添加如下代码：

```go
package model

type Todo struct {
	ID   string
	Text string
	Done bool
	User *User
}
```

重新生成代码，你会发现 `models_gen.go` 中的 `Todo` 模型没了，然后到 `generated.go` 文件中去查看其中的 `model.Todo` 模型的定义，会发现这个模型已经替换成了我们自己添加的模型。gqlgen 怎么做到的呢？

那就是配置文件中 `autobind` 配置的作用，gqlgen 会到 `autobind` 配置的包中根据模型名匹配来绑定模型。如果没找到同名模型再生成模型。

但是生产环境下，我们一般不用 `autobind` 而是使用如下方式绑定模型：

```yml
models:
  Todo:
    model:
      - gitlab.com/mvalley/gqlgen-todo/graph/model.Todo
```

直接配置 schema 中的 `Todo` 绑定 `gitlab.com/mvalley/gqlgen-todo/graph/model.Todo` 这个模型。这种绑定方式模型可以不同名，比如你可以把模型名改为 `MyTodo`，然后配置文件改成如下：

```yml
models:
  Todo:
    model:
      - gitlab.com/mvalley/gqlgen-todo/graph/model.MyTodo
```

即可让 `Todo` 绑定我们的 `MyTodo` 模型。

模型绑定还能支持绑定字段名不匹配的，绑定某个字段到一个方法等情况，详情请看 gqlgen 文档 [Resolvers](https://gqlgen.com/reference/resolvers/) 章节。

### 1.7. 自定义标量

当 GraphQL 提供的 int64，string，bool 等基础类型无法满足我们需要时，我们可以自己定义类型，定义后只需要实现其序列化与反序列化方法即可使用。

gqlgen 中已经实现了一些比较常用的标量，比如 Time, Any 以及 Map 等。分别对应 Go 中的如下类型：

```text
Time -> time.Time
Any -> interface{}
Map -> map[string]interface{}
```

我们来使用下 `Time` 标量。在 `.graphql` 文件中添加如下代码：

```graphql
scalar Time
```

然后修改 `Todo` 如下：

```graphql
type Todo {
  id: ID!
  text: String!
  done: Boolean!
  doneAt: Time # 添加了完成时间字段
  user: User!
}
```

相应的，我们修改 Go 模型为：

```go
type MyTodo struct {
  ID     string
  Text   string
  Done   bool
  DoneAt time.Time
  User   *User
}
```

再次重新生成代码。没有报错，绑定成功。我们再来看看效果。

修改 `CreateTodo` resolver 实现如下：

```go
func (r *mutationResolver) CreateTodo(ctx context.Context, input model.NewTodo) (*model.MyTodo, error) {
	todo := &model.MyTodo{
		Text: input.Text,
		ID:   fmt.Sprintf("T%d", rand.Int()),
		User: &model.User{ID: input.UserID, Name: "user " + input.UserID},
		DoneAt: time.Now(),  // 添加了这一行
	}
	r.todos = append(r.todos, todo)
	return todo, nil
}
```

再到 Playground 中添加一个 TODO 后查询 todos，即可看到返回中有 doneAt。

接下来我们来自己定义一个 `MyBoolean` 标量。

1. 在 schema 中定义标量并使用标量：

```graphql
scalar MyBoolean # 定义标量

type Todo {
  id: ID!
  text: String!
  done: MyBoolean! # 使用自定义标量
  doneAt: Time # 完成时间
  user: User!
}

input NewTodo {
  text: String!
  done: MyBoolean! # 使用自定义标量
  userId: String!
}
```

2. 实现 `MyBoolean` 序列化与反序列化方法，在 `model` 目录下添加 `my_boolean.go` 文件并添加如下代码：

```go
package model

import (
	"fmt"
	"io"
)

type MyBoolean bool

// UnmarshalGQL implements the graphql.Unmarshaler interface
func (y *MyBoolean) UnmarshalGQL(v interface{}) error {
	yes, ok := v.(string)
	if !ok {
		return fmt.Errorf("points must be strings")
	}

	if yes == "yes" {
		*y = true
	} else {
		*y = false
	}
	return nil
}

// MarshalGQL implements the graphql.Marshaler interface
func (y MyBoolean) MarshalGQL(w io.Writer) {
	if y {
		w.Write([]byte(`"yes"`))
	} else {
		w.Write([]byte(`"no"`))
	}
}
```

3. 绑定模型，在配置中添加模型绑定配置：

```yml
models:
  MyBoolean:
    model:
      - gitlab.com/mvalley/gqlgen-todo/graph/model.MyBoolean
```

4. 修改 `MyTodo` 模型中 `done` 字段的类型为 `MyBoolean` 并重新生成代码

5. 最后在 `CreateTodo` 实现中赋值 `done` 字段。最后达到的效果应该是创建 TODO 时可以输入 `done: yes` 参数，返回的 `done` 应该是 `"yes"` 字符串

参考 gqlgen 文档 [Scalars](https://gqlgen.com/reference/scalars/)

### 1.8. 官方文档 & Github
[官方文档](https://gqlgen.com/)

[Github](https://github.com/99designs/gqlgen)

### 1.9. gqlgen 使用常见的“坑”

1. gqlgen 的枚举在 yaml 文件中绑定后，还需要实现序列化和反序列化方法。

## 2. Twirp

### 2.1. RPC 是什么？

在学习 Twirp 前我们先了解下 RPC（Remote Procedure Call）

RPC 是远程过程调用，也就是让客户端像调用本地方法一样去调用远程服务端的方法。

调用过程如下：

Client
1. 将过程调用映射为 Call ID。
2. 将 Call ID 以及 params 序列化，以二进制形式打包
3. 把 2 中得到的数据包通过网络传输发送给 ServerAddr
4. 等待服务器返回结果
5. 如果服务器调用成功，那么反序列化后便得到结果

Server 
1. 在本地维护一个 Call ID 到函数指针的映射，类似 CallIDMap Map<String, Method>
2. 等待服务端请求
3. 得到一个请求后，将其数据包反序列化，得到 Call ID
4. 通过在 CallIDMap 中查找，得到相应的函数指针
5. 将 params 反序列化后，在本地调用函数，得到结果
6. 将结果序列化后通过网络返回给 Client

如下图：

![RPC](./rpc.webp)

### 2.2. Twirp 是什么？

Twirp 是一个简单的基于 protobuf 的 RPC 框架。

### 2.3. 为什么选择 Twirp

Twirp 可以说是继承了 gRPC 优点，规避了 gRPC 的不足

| 功能 | Twirp | gRPC |
|:------:|:------:|:------:|
| http 协议 | http1.1 以及 http2 | http2 |
| 序列化协议 | Protobuf & JSON | Protobuf |
| 自动生成接口代码 | 支持 | 支持 |

### 2.4. 环境配置

- 安装 Protoc Compiler
```bash
sudo apt-get update
sudo apt install protobuf-compiler
```

- 安装 protoc-gen-go 和 protoc-gen-twirp 插件
```bash
go get -u github.com/golang/protobuf/protoc-gen-go
go get -u github.com/twitchtv/twirp/protoc-gen-twirp
```

- 确定 PATH 环境变量中包含了 $GOPATH/bin
```
export PATH=$PATH:$GOPATH/bin
```

### 2.5. 快速开始

1. 初始化一个示例项目：

	```bash
	$ mkdir twirp-todos
	$ cd twirp-todos
	$ go mod init gitlab.com/mvalley/twirp-todos
	```

2. 定义 RPC 服务：

	添加文件夹 `rpc/twirp-todos`，在文件夹中新建 `service.proto` 文件，并将如下代码添加到文件中

	```protobuf
	syntax = "proto3";

	package io.mvalley.todos;

	import "google/protobuf/empty.proto";
	option go_package = "twirp-todos";

	service Todos {
	rpc CreateTodo(CreateTodoRequest) returns (Todo);
	rpc GetTodos(google.protobuf.Empty) returns (GetTodosResponse);
	}

	message CreateTodoRequest {
		string text = 1;
		string userID = 2;
	}

	message Todo {
		string id = 1;
		string text = 2;
		bool done = 3;
		User user = 4;
	}

	message User {
		string id = 1;
		string name = 2;
	}

	message GetTodosResponse {
		repeated Todo todos = 1;
	}
	```

3. 生成代码：

	执行命令
	```bash
	protoc --proto_path=$GOPATH/src:. --twirp_out=. --go_out=. ./rpc/todos/service.proto
	```

	可以看到 `rpc/twirp-todos` 下面多出了 `service.pb.go` 以及 `service.twirp.go` 两个文件。

	- 从 `service.pb.go` 文件中我们能找到我们通过 `proto` 定义的模型
	- 从 `service.twirp.go` 文件中我们能找到我们定义的 RPC 接口

4. 实现接口：

	添加文件夹 `internal/server`，在文件夹中新建 `server.go` 文件，并将如下代码添加到文件中

	```go
	package server

	import (
		"context"
		"fmt"
		"math/rand"

		"github.com/golang/protobuf/ptypes/empty"
		pb "gitlab.com/mvalley/twirp-todos/rpc/twirp-todos"
	)

	// Server ...
	type Server struct {
		todos []*pb.Todo
	}

	// CreateTodo ...
	func (s *Server) CreateTodo(ctx context.Context, req *pb.CreateTodoRequest) (*pb.Todo, error) {
		todo := &pb.Todo{
			Text: req.Text,
			Id:   fmt.Sprintf("T%d", rand.Int()),
			User: &pb.User{Id: req.UserID, Name: "user " + req.UserID},
		}
		s.todos = append(s.todos, todo)
		return todo, nil
	}

	// GetTodos ...
	func (s *Server) GetTodos(ctx context.Context, req *empty.Empty) (*pb.GetTodosResponse, error) {
		res := &pb.GetTodosResponse{
			Todos: s.todos,
		}
		return res, nil
	}
	```

5. 挂载并运行服务：

	添加文件夹 `cmd/twirp-todos`，在文件夹中新建 `main.go` 文件，并将如下代码添加到文件中

	```go
	package main

	import (
		"log"
		"net/http"

		"gitlab.com/mvalley/twirp-todos/internal/server"
		pb "gitlab.com/mvalley/twirp-todos/rpc/twirp-todos"
	)

	func main() {
		server := &server.Server{}
		twirpHandler := pb.NewTodosServer(server, nil)

		log.Println("server on")
		http.ListenAndServe(":8080", twirpHandler)

	}
	```

	执行 `go run cmd/twirp-todos/main.go` 即可启动服务

6. 测试服务：

	添加文件夹 `test/twirp-todos/client`，在文件夹中新建 `main.go` 文件，并将如下代码添加到文件中

	```go
	package main

	import (
		"context"
		"encoding/json"
		"fmt"
		"log"
		"net/http"
		"os"

		pb "gitlab.com/mvalley/twirp-todos/rpc/twirp-todos"
		"google.golang.org/protobuf/types/known/emptypb"
	)

	func main() {
		client := pb.NewTodosJSONClient("http://localhost:8080", &http.Client{})

		todo, err := client.CreateTodo(context.Background(), &pb.CreateTodoRequest{
			Text:   "new todo",
			UserID: "yzl",
		})
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}

		todoJSON, err := json.MarshalIndent(*todo, "", "\t")
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}
		fmt.Println(string(todoJSON))

		fmt.Println("===============")

		todos, err := client.GetTodos(context.Background(), &emptypb.Empty{})
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}
		todosJSON, err := json.MarshalIndent(*todos, "", "\t")
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}
		fmt.Println(string(todosJSON))
	}
	```

	新建一个终端并执行 `go run test/twirp-todos/client/main.go` 即可看到创建一个 TODO 并输出 TODO 列表

### 2.6. 官方文档 & Github

[官方文档](https://twitchtv.github.io/twirp/docs/intro.html)

[Github](https://github.com/twitchtv/twirp)

### 2.7. Twirp 使用常见的“坑”

TODO

## 3. Protobuf

### 3.1. 是什么？

是一种轻便高效的结构化数据存储与传输的格式（类似JSON和XML）。具有语言无关、平台无关、高效、扩展性以及兼容性好等优点。可用于（数据）通信协议、数据存储等。

### 3.2. Defining A Message Type

```protobuf
syntax = "proto3";

message CreateTodoRequest {
    string text = 1;
    string userID = 2;
}
```

### 3.3. Using Other Message Types

```protobuf
message Todo {
    string id = 1;
    string text = 2;
    bool done = 3;
    User user = 4;
}

message User {
    string id = 1;
    string name = 2;
}
```

### 3.4. Importing Definitions

```protobuf
import "google/protobuf/empty.proto";

service Todos {
  rpc CreateTodo(CreateTodoRequest) returns (Todo);
  rpc GetTodos(google.protobuf.Empty) returns (GetTodosResponse);
}
```

### 3.5. Defining Services

```protobuf
service Todos {
  rpc CreateTodo(CreateTodoRequest) returns (Todo);
  rpc GetTodos(google.protobuf.Empty) returns (GetTodosResponse);
}
```

### 3.6. 官方文档 & Github
[官方文档](https://developers.google.cn/protocol-buffers/docs/proto3)

[Github](https://github.com/golang/protobuf/)

### 3.7. Protobuf 使用常见的“坑”

1. rpc 服务名不能和 message 定义冲突

## 4. gqlgen & Twirp

### 4.1. RPC 和微服务
在微服务的理念下，各个相对独立的业务逻辑被拆分为许多单独的服务。服务之间如果有业务需要，请求方会通过 RPC、REST、或其他手段向服务端提出请求。这是后端微服务之间的调用模式。

Remote Procedure Call 就是“调用远程服务器上的方法”。由于其性能优异，经常被选为微服务接口的实现方式。我们学习 Twirp 就是为了让我们的后端微服务之间通过 RPC 接口通信。

### 4.2. API Gateway

在微服务的理念中，如果前端想要对后端进行请求，需要为每个微服务单独开发一套客户端。这不仅在开发时耗费时间，而且对这些客户端的管理也极其繁琐。因此，我们可以提出一个 API Gateway 的概念。

如下图：
![API Gateway](./gateway-routing.png)

网关服务接收前端请求，然后分发到相应的微服务做业务处理，处理结果返回后再传回给前端。

这种模式的优点很多，但是也存在一些显著的缺点：

1. 单点故障
2. 网关服务会带来瓶颈

#### 4.2.1. 参考文档

[gateway-routing](https://docs.microsoft.com/en-us/azure/architecture/patterns/gateway-routing)

#### 4.2.2. 一个具体案例

我们看一个具体案例

[案例](https://outcrawl.com/go-graphql-gateway-microservices)

看的时候关注以下几点：
1. 作者拆分服务的方法。
2. 各个 RPC 微服务的定义，以及 Gateway GraphQL 的 Schema 定义。
3. GraphQL resolver 调用微服务的具体代码。

### 4.3. 课间练习

参考 API Gateway 章节的案例，将我们之前示例的 todos 改成 API Gateway 模式。

时间： 1 小时

### 4.4. 共用模型

请查看案例中的这段代码：

```go
func (s *GraphQLServer) Mutation_createAccount(ctx context.Context, in AccountInput) (*Account, error) {
  ctx, cancel := context.WithTimeout(ctx, 3*time.Second)
  defer cancel()

  a, err := s.accountClient.PostAccount(ctx, in.Name)
  if err != nil {
    log.Println(err)
    return nil, err
  }

  return &Account{
    ID:   a.ID,
    Name: a.Name,
  }, nil
}
```

我们注意到，使用 Gateway 架构的后果，是需要维护两套模型。一套是 GraphQL 的模型，比如 gqlgen 为我们自动生成的模型。另一套是 RPC 框架的模型，比如 Twirp 为我们生成的 .pb.go 文件中定义的模型。在 Gateway 的 resolver 中，不仅需要调用 RPC，还需要将 RPC 返回的结果转编码 (Transcode) 为 GraphQL 模型。也就是说，Gateway 中最繁重的工作是 Transcoding，特别是当我们的模型有多层嵌套的时候。

如果我们能够让 gqlgen 和 Twirp 共用一套模型的话，那么就可以节约大量的开发成本，也有助于提高 Gateway 的效率。那么如何做到这一点呢？

我们首先需要定义 GraphQL Schema，换句话说，是定义业务逻辑。再通过 proto 文件定义我们的 RPC 服务，其中遵循一个原则：

**proto 中返回结果的结构体要与对应的 GraphQL query/mutation 返回的结构体一致，避免繁琐的 Transcoding。**

比如，请看 [example.proto](./example.proto) 和 [example.graphql](./example.graphql)。在 GraphQL 中，suggestions 返回的结构体和 proto 中 GetSuggestions 返回的结构体是完全一致的。这意味着，我们只需要在 gqlgen 的配置文件中绑定好生成的 .pb.go 文件里的模型，就可以避免 Transcoding，直接返回 RPC 服务端传回来的结果。

值得注意的是：
1. RPC 定义的服务端接口并不需要和 GraphQL 定义的完全一致。比如，例子中的 RPC GetSuggestions 只实现了 first, after 参数，但 GraphQL 中的 suggestions 却有 last, before 参数。这种情况下，我们可以再定义一个实现了 last, before 的 RPC 服务，并在 Gateway 的 resolver 中做一个判断，如果用户使用了 last, before 参数，那么就调用新定义的 RPC 接口。也就是说，RPC 中的接口数量理应比 GraphQL 中定义的接口数量多。
2. 不同于 GraphQL，proto 的输入参数只能是一个。如果有多个参数，那么只能打包在一个 Message 里。因此我们无需过多关注两者间输入参数的模型一致性，而是要重点关注返回结果的模型一致。
3. proto 生成的模型会比 GraphQL 中的模型多出一些东西，但这无伤大雅。比如：
```go
type PageInfo struct {
	HasPreviousPage      bool                  `protobuf:"varint,1,opt,name=has_previous_page,json=hasPreviousPage,proto3" json:"has_previous_page,omitempty"`
	HasNextPage          bool                  `protobuf:"varint,2,opt,name=has_next_page,json=hasNextPage,proto3" json:"has_next_page,omitempty"`
	StartCursor          *wrappers.StringValue `protobuf:"bytes,3,opt,name=start_cursor,json=startCursor,proto3" json:"start_cursor,omitempty"`
	EndCursor            *wrappers.StringValue `protobuf:"bytes,4,opt,name=end_cursor,json=endCursor,proto3" json:"end_cursor,omitempty"`
	XXX_NoUnkeyedLiteral struct{}              `json:"-"`
	XXX_unrecognized     []byte                `json:"-"`
	XXX_sizecache        int32                 `json:"-"`
}
```

### 4.5. proto Wrapper Values
我们现在已经定义好了 GraphQL schema 和 proto RPC 服务，也写好了 gqlgen 模型绑定配置。现在可以开始生成代码了……

1. 用 protoc 生成 RPC 代码。
2. 用 gqlgen 生成相关的代码，并且应该绑定到 .pb.go 中的模型。

但我们遇到了几个问题：
1. 基本类型不匹配。protoc 中生成的模型里的各个结构体成员类型，不一定符合 gqlgen 的绑定要求。比如，GraphQL 中的 Float 对应为 go 语言的 float 类型；但 proto 中的 float 类型对应的是 go 语言中的 float32。在我们生成 gqlgen 代码的时候，会有这样的错误提示：
```
Float is incompatible with float32
```
2. proto3 中，参数无 required 和 optional 的概念。我们可以找一个 gqlgen 生成的原生模型作为例子。所有 optional field，最后都会对应为一个指针。但 protoc 生成的模型，除非对象是 message，否则不会对应为指针。这意味着，如果使用 protoc 生成的模型，前端将无法辨别“空值”和“默认值”。

为了解决第一个问题，我们需要定义一套 GraphQL 的 scalar type，比如：
```graphql
scalar Float32
scalar Int32
```
然后替换原本的类型，比如:
```graphql
# Node for SuggestionEdge
type Suggestion {
    id: String!
    suggestType: [SuggestType!]!
    entityType: String
    score: Float32!                // 注意这里
    matchedNames: [String]!
    primaryName: String!
    description: String
}
```
紧接着，我们需要为这个新 Scalar Type 定义 gqlgen 的 Marsharler 和 Unmarshaler。参考 gqlgen 的对应[文档](https://gqlgen.com/reference/resolvers/)：
```go
// MarshalFloat32 ...
func MarshalFloat32(any float32) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		_, _ = w.Write([]byte(strconv.Itoa(int(any))))
	})
}

// UnmarshalFloat32 ...
func UnmarshalFloat32(v interface{}) (float32, error) {
	switch v := v.(type) {
	case int:
		return float32(v), nil
	case float32:
		return v, nil
	case json.Number:
		f, err := v.Float64()
		return float32(f), err
	default:
		return 0, fmt.Errorf("%T is not float32", v)
	}
}
```
最后在 gqlgen 中配置模型绑定即可。

第二个问题的核心矛盾在于，由于没有 required 和 optional，无法区分默认值和空值。我们可以使用谷歌的[封装类型]("https://github.com/protocolbuffers/protobuf/blob/master/src/google/protobuf/wrappers.proto")解决这个问题。这个文件中定义了很多 message 类型，其中只有一个 Value 字段。比如：
```protobuf
message StringValue {
  // The String value.
  string value = 1;
}
```
我们可以在自己的 proto 文件中使用他们，如下：
```
import "google/protobuf/wrappers.proto";

message PageInfo {
    bool has_previous_page = 1;
    bool has_next_page = 2;
    google.protobuf.StringValue start_cursor = 3;
    google.protobuf.StringValue end_cursor = 4;
}
```
由于 protoc 会将 message 转化为一个指向对应结构体的指针，我们可以通过把这个指针设为 nil 来表达空值，以区分默认值。

但这也意味着，我们需要另外定义 GraphQL Scalar，来表示可选值。在编程语言中，我们把它称作 Maybe 类型。
```graphql
scalar MaybeString
```
并且将所有 optional field 用 Maybe Scalar 来表示：
```graphql
type PageInfo {
    hasPreviousPage: Boolean!
    hasNextPage: Boolean!
    startCursor: MaybeString
    endCursor: MaybeString
}
```
当然，也要定义好 Marshaler 和 Unmarshaler：
```go
require "github.com/golang/protobuf/ptypes/wrappers"

// MarshalMaybeString ...
func MarshalMaybeString(s wrappers.StringValue) graphql.Marshaler {
	return graphql.WriterFunc(func(w io.Writer) {
		io.WriteString(w, fmt.Sprintf("\"%s\"", s.Value))
	})
}

// UnmarshalMaybeString ...
func UnmarshalMaybeString(v interface{}) (wrappers.StringValue, error) {
	switch v := v.(type) {
	case string:
		return wrappers.StringValue{Value: v}, nil
	default:
		return wrappers.StringValue{Value: ""}, fmt.Errorf("%T is not a StringValue", v)
	}
}
```
注意，这里我们用 MaybeString 对应了第三方 wrapper 库中的 StringValue 类型。在 gqlgen 模型绑定中，要记得绑定正确的名称。

如此以来，两个问题就都解决了。唯一繁琐的部分，就是需要统一使用自定义的 scalar type。我们已经将常用的类型封装至了这个[项目](https://gitlab.com/momentum-valley/proto-gql)，可酌情使用。总结一下：

1. GraphQL 中，凡是遇到 optional field，都需要使用 Maybe 类型（不带感叹号）。凡是遇到 required field，按照实际情况酌情使用自定义的类型（带感叹号）。一切以 gqlgen 能够顺利绑定类型为准。
2. proto 中，凡是 optional field，都需要使用 google 提供的 wrapper 类型。
3. proto 中，比如 oneof 和 any 类型，我们暂时还未使用到。今后用到再找具体方案。


