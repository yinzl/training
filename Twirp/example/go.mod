module gitlab.com/mvalley/twirp-todos

go 1.13

require (
	github.com/golang/protobuf v1.4.2
	github.com/twitchtv/twirp v5.12.0+incompatible
)
