package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"

	pb "gitlab.com/mvalley/twirp-todos/rpc/twirp-todos"
	"google.golang.org/protobuf/types/known/emptypb"
)

func main() {
	client := pb.NewTodosJSONClient("http://localhost:8080", &http.Client{})

	todo, err := client.CreateTodo(context.Background(), &pb.CreateTodoRequest{
		Text:   "new todo",
		UserID: "yzl",
	})
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	todoJSON, err := json.MarshalIndent(*todo, "", "\t")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	fmt.Println(string(todoJSON))

	fmt.Println("===============")

	todos, err := client.GetTodos(context.Background(), &emptypb.Empty{})
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	todosJSON, err := json.MarshalIndent(*todos, "", "\t")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	fmt.Println(string(todosJSON))
}
