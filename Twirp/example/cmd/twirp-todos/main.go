package main

import (
	"log"
	"net/http"

	"gitlab.com/mvalley/twirp-todos/internal/server"
	pb "gitlab.com/mvalley/twirp-todos/rpc/twirp-todos"
)

func main() {
	server := &server.Server{}
	twirpHandler := pb.NewTodosServer(server, nil)

	log.Println("server on")
	http.ListenAndServe(":8080", twirpHandler)

}
