package server

import (
	"context"
	"fmt"
	"math/rand"

	"github.com/golang/protobuf/ptypes/empty"
	pb "gitlab.com/mvalley/twirp-todos/rpc/twirp-todos"
)

// Server ...
type Server struct {
	todos []*pb.Todo
}

// CreateTodo ...
func (s *Server) CreateTodo(ctx context.Context, req *pb.CreateTodoRequest) (*pb.Todo, error) {
	todo := &pb.Todo{
		Text: req.Text,
		Id:   fmt.Sprintf("T%d", rand.Int()),
		User: &pb.User{Id: req.UserID, Name: "user " + req.UserID},
	}
	s.todos = append(s.todos, todo)
	return todo, nil
}

// GetTodos ...
func (s *Server) GetTodos(ctx context.Context, req *empty.Empty) (*pb.GetTodosResponse, error) {
	res := &pb.GetTodosResponse{
		Todos: s.todos,
	}
	return res, nil
}
